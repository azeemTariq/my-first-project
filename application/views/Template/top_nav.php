  <header>
        <div class="header-area">
            <div class="header-left-sidebar">
                <div class="logo">
                    <a href="index.html"><img src="assets/img/logo/logo.png" alt=""></a>
                </div>
                <div class="main-menu menu-hover">
                    <nav>
                        <ul>
                            <li><a href="#">Home</a>
                                <ul class="single-dropdown">
                                    <li><a href="index.html">Fashion</a></li>
                                    <li><a href="index-fashion-2.html">Fashion style 2</a></li>
                                    <li><a href="index-fruits.html">fruits</a></li>
                                    <li><a href="index-book.html">book</a></li>
                                    <li><a href="index-electronics.html">electronics</a></li>
                                    <li><a href="index-electronics-2.html">electronics style 2</a></li>
                                    <li><a href="index-food.html">food & drink</a></li>
                                    <li><a href="index-furniture.html">furniture</a></li>
                                    <li><a href="index-handicraft.html">handicraft</a></li>
                                    <li><a target="_blank" href="index-smart-watch.html">smart watch</a></li>
                                    <li><a href="index-sports.html">sports</a></li>
                                </ul>
                            </li>
                            <li><a href="#">Pages</a>
                                <!-- <ul class="single-dropdown">
                                    <li><a href="about-us.html">about us</a></li>
                                    <li><a href="menu-list.html">menu list</a></li>
                                    <li><a href="login.html">login</a></li>
                                    <li><a href="register.html">register</a></li>
                                    <li><a href="cart.html">cart page</a></li>
                                    <li><a href="checkout.html">checkout</a></li>
                                    <li><a href="wishlist.html">wishlist</a></li>
                                    <li><a href="contact.html">contact</a></li>
                                </ul> -->
                            </li>
                         <!--    <li><a href="#">Latest <span>New</span></a>
                                <div class="mega-menu-dropdown mega-dropdown-width">
                                    <div class="mega-dropdown-style mega-common4 mb-40">
                                        <h4 class="mega-subtitle"> Clothing</h4>
                                        <ul>
                                            <li><a href="#"> New Products</a></li>
                                            <li><a href="#"> Jackets</a></li>
                                            <li><a href="#"> Dress</a></li>
                                            <li><a href="#"> Winter Collection</a></li>
                                            <li><a href="#"> Ladis Jeans</a></li>
                                            <li><a href="#"> Multipacks</a></li>
                                            <li><a href="#"> Shorts</a></li>
                                            <li><a href="#"> Night wear</a></li>
                                            <li><a href="#"> Top Products</a></li>
                                        </ul>
                                    </div>
                                    <div class="mega-dropdown-style mega-common4 mb-40">
                                        <h4 class="mega-subtitle"> New Products</h4>
                                        <ul>
                                            <li><a href="#">View All</a></li>
                                            <li><a href="#">New Trand</a></li>
                                            <li><a href="#">Boots</a></li>
                                            <li><a href="#">Flat Shoes</a></li>
                                            <li><a href="#">Women Heels</a></li>
                                            <li><a href="#">Slippers</a></li>
                                            <li><a href="#">Socks & Tights</a></li>
                                            <li><a href="#">Trainers</a></li>
                                        </ul>
                                    </div>
                                    <div class="mega-dropdown-style mega-common4 mb-40">
                                        <h4 class="mega-subtitle">Tranding</h4>
                                        <ul>
                                            <li><a href="#">Weeding</a></li>
                                            <li><a href="#">Winter</a></li>
                                            <li><a href="#">Holidays</a></li>
                                            <li><a href="#">Night Party</a></li>
                                            <li><a href="#">Outing Dress</a></li>
                                            <li><a href="#">Outing Dress</a></li>
                                        </ul>
                                    </div>
                                    <div class="mega-dropdown-style mega-common4 discount-mega-common4 mb-40">
                                        <div class="mega-discount">
                                            <h5>Make A Discount</h5>
                                            <h2>UP TO 30%</h2>
                                        </div>
                                    </div>
                                    <div class="mega-banner-img-2">
                                        <a href="single-product.html"><img src="assets/img/bg/3.png" alt=""></a>
                                    </div>
                                </div>
                            </li>
                            <li><a href="#">Shop</a>
                                <div class="mega-menu-dropdown mega-dropdown-width-2">
                                    <div class="mega-dropdown-style mega-common2 mega-common4">
                                        <h4 class="mega-subtitle"> shop layout</h4>
                                        <ul>
                                            <li><a href="shop-grid-2-col.html"> grid 2 column</a></li>
                                            <li><a href="shop-grid-3-col.html"> grid 3 column</a></li>
                                            <li><a href="shop.html">grid 4 column</a></li>
                                            <li><a href="shop-grid-box.html">grid box style</a></li>
                                            <li><a href="shop-list-1-col.html"> list 1 column</a></li>
                                            <li><a href="shop-list-2-col.html">list 2 column</a></li>
                                            <li><a href="shop-list-box.html">list box style</a></li>
                                            <li><a href="cart.html">shopping cart</a></li>
                                            <li><a href="wishlist.html">wishlist</a></li>
                                        </ul>
                                    </div>
                                    <div class="mega-dropdown-style mega-common2 mega-common4">
                                        <h4 class="mega-subtitle">product details</h4>
                                        <ul>
                                            <li><a href="product-details.html">tab style 1</a></li>
                                            <li><a href="product-details-2.html">tab style 2</a></li>
                                            <li><a href="product-details-3.html"> tab style 3</a></li>
                                            <li><a href="product-details-4.html">sticky style</a></li>
                                            <li><a href="product-details-5.html">sticky style 2</a></li>
                                            <li><a href="product-details-6.html">gallery style</a></li>
                                            <li><a href="product-details-7.html">gallery style 2</a></li>
                                            <li><a href="product-details-8.html">fixed image style</a></li>
                                            <li><a href="product-details-9.html">fixed image style 2</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <li><a href="#">Kids</a>
                            </li> -->
                            <!-- <li><a href="#">Discount</a></li>
                            <li><a href="#">Health <span>New</span></a></li> -->
                            <li><a href="contact.html">contact</a></li>
                        </ul>
                    </nav>
                </div>
                <div class="currency">
                    <ul>
                        <li><a href="#">EN</a></li>
                        <li><a href="#">RN</a></li>
                    </ul>
                </div>
            </div>
            <div class="header-right-sidebar">
                <div class="header-search-cart-login">
                    <div class="logo">
                        <a href="index.html">
                            <img src="assets/img/logo/logo.png" alt="">
                        </a>
                    </div>
                    <div class="header-search">
                        <form action="#">
                            <input placeholder="Search What you want" type="text">
                            <button>
                                <i class="ti-search"></i>
                            </button>
                        </form>
                    </div>
                    <div class="header-login">
                        <ul>
                            <li><a href="login.html">Login</a></li>
                            <li><a href="register.html">Reg</a></li>
                        </ul>
                    </div>
                    <div class="header-cart cart-res">
                        <a class="icon-cart" href="#">
                            <i class="ti-shopping-cart"></i>
                            <span class="shop-count pink">02</span>
                        </a>
                        <ul class="cart-dropdown">
                            <li class="single-product-cart">
                                <div class="cart-img">
                                    <a href="#"><img src="assets/img/cart/1.jpg" alt=""></a>
                                </div>
                                <div class="cart-title">
                                    <h5><a href="#"> Bits Headphone</a></h5>
                                    <h6><a href="#">Black</a></h6>
                                    <span>$80.00 x 1</span>
                                </div>
                                <div class="cart-delete">
                                    <a href="#"><i class="ti-trash"></i></a>
                                </div>
                            </li>
                            <li class="single-product-cart">
                                <div class="cart-img">
                                    <a href="#"><img src="assets/img/cart/2.jpg" alt=""></a>
                                </div>
                                <div class="cart-title">
                                    <h5><a href="#"> Bits Headphone</a></h5>
                                    <h6><a href="#">Black</a></h6>
                                    <span>$80.00 x 1</span>
                                </div>
                                <div class="cart-delete">
                                    <a href="#"><i class="ti-trash"></i></a>
                                </div>
                            </li>
                            <li class="single-product-cart">
                                <div class="cart-img">
                                    <a href="#"><img src="assets/img/cart/3.jpg" alt=""></a>
                                </div>
                                <div class="cart-title">
                                    <h5><a href="#"> Bits Headphone</a></h5>
                                    <h6><a href="#">Black</a></h6>
                                    <span>$80.00 x 1</span>
                                </div>
                                <div class="cart-delete">
                                    <a href="#"><i class="ti-trash"></i></a>
                                </div>
                            </li>
                            <li class="cart-space">
                                <div class="cart-sub">
                                    <h4>Subtotal</h4>
                                </div>
                                <div class="cart-price">
                                    <h4>$240.00</h4>
                                </div>
                            </li>
                            <li class="cart-btn-wrapper">
                                <a class="cart-btn btn-hover" href="#">view cart</a>
                                <a class="cart-btn btn-hover" href="#">checkout</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="mobile-menu-area clearfix d-md-block col-md-12 col-lg-12 col-12 d-lg-none d-xl-none">
                    <div class="mobile-menu">
                        <nav id="mobile-menu-active">
                            <ul class="menu-overflow">
                                <li><a href="#">HOME</a>
                                    <ul>
                                        <li><a href="index.html">Fashion</a></li>
                                        <li><a href="index-fashion-2.html">Fashion style 2</a></li>
                                        <li><a href="index-fruits.html">Fruits</a></li>
                                        <li><a href="index-book.html">book</a></li>
                                        <li><a href="index-electronics.html">electronics</a></li>
                                        <li><a href="index-electronics-2.html">electronics style 2</a></li>
                                        <li><a href="index-food.html">food & drink</a></li>
                                        <li><a href="index-furniture.html">furniture</a></li>
                                        <li><a href="index-handicraft.html">handicraft</a></li>
                                        <li><a href="index-smart-watch.html">smart watch</a></li>
                                        <li><a href="index-sports.html">sports</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">pages</a>
                                    <ul>
                                        <li><a href="about-us.html">about us</a></li>
                                        <li><a href="menu-list.html">menu list</a></li>
                                        <li><a href="login.html">login</a></li>
                                        <li><a href="register.html">register</a></li>
                                        <li><a href="cart.html">cart page</a></li>
                                        <li><a href="checkout.html">checkout</a></li>
                                        <li><a href="wishlist.html">wishlist</a></li>
                                        <li><a href="contact.html">contact</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">shop</a>
                                    <ul>
                                        <li><a href="shop-grid-2-col.html"> grid 2 column</a></li>
                                        <li><a href="shop-grid-3-col.html"> grid 3 column</a></li>
                                        <li><a href="shop.html">grid 4 column</a></li>
                                        <li><a href="shop-grid-box.html">grid box style</a></li>
                                        <li><a href="shop-list-1-col.html"> list 1 column</a></li>
                                        <li><a href="shop-list-2-col.html">list 2 column</a></li>
                                        <li><a href="shop-list-box.html">list box style</a></li>
                                        <li><a href="product-details.html">tab style 1</a></li>
                                        <li><a href="product-details-2.html">tab style 2</a></li>
                                        <li><a href="product-details-3.html"> tab style 3</a></li>
                                        <li><a href="product-details-4.html">sticky style</a></li>
                                        <li><a href="product-details-5.html">sticky style 2</a></li>
                                        <li><a href="product-details-6.html">gallery style</a></li>
                                        <li><a href="product-details-7.html">gallery style 2</a></li>
                                        <li><a href="product-details-8.html">fixed image style</a></li>
                                        <li><a href="product-details-9.html">fixed image style 2</a></li>
                                    </ul>
                                </li>
                                <li><a href="#">BLOG</a>
                                    <ul>
                                        <li><a href="blog.html">blog 3 colunm</a></li>
                                        <li><a href="blog-2-col.html">blog 2 colunm</a></li>
                                        <li><a href="blog-sidebar.html">blog sidebar</a></li>
                                        <li><a href="blog-details.html">blog details</a></li>
                                        <li><a href="blog-details-sidebar.html">blog details 2</a></li>
                                    </ul>
                                </li>
                                <li><a href="contact.html"> Contact  </a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="slider-area ">
                    <div class="slider-active owl-carousel">
                        <div class="single-slider single-slider-hm1 bg-img height-100vh" style="background-image: url(assets/img/slider/15.jpg)">
                            <div class="slider-content slider-animation slider-content-style-1 slider-animated-1">
                                <h1 class="animated">Fashion</h1>
                                <p class="animated">Create you own style for better looks. </p>
                            </div>
                            <div class="position-slider-img">
                                <div class="slider-img-1">
                                    <img src="assets/img/slider/9.png" alt="">
                                </div>
                                <div class="slider-img-2">
                                    <img class="tilter" src="assets/img/slider/7.png" alt="">
                                </div>
                                <div class="slider-img-3">
                                    <img src="assets/img/slider/8.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="single-slider single-slider-hm1 bg-img height-100vh" style="background-image: url(assets/img/slider/15.jpg)">
                            <div class="slider-content slider-animation slider-content-style-1 slider-animated-2">
                                <h1 class="animated">Fashion</h1>
                                <p class="animated">Create you own style for better looks. </p>
                            </div>
                            <div class="position-slider-img">
                                <div class="slider-img-1">
                                    <img src="assets/img/slider/9.png" alt="">
                                </div>
                                <div class="slider-img-4 slider-mrg">
                                    <img class="tilter" src="assets/img/slider/10.png" alt="">
                                </div>
                                <div class="slider-img-3">
                                    <img src="assets/img/slider/8.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>