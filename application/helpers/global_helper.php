<?php


if (! defined('BASEPATH')) exit('No direct script access allowed');
 

function encrypt($pure_string) {
    $dirty = array("+", "/", "=");
    $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
    $encrypted_string = base64_encode(json_encode($pure_string));
    return str_replace($dirty, $clean, $encrypted_string);
}

function decrypt($encrypted_string) { 
    $dirty = array("+", "/", "=");
    $clean = array("_PLUS_", "_SLASH_", "_EQUALS_");
    $decrypted_string = json_decode( base64_decode(str_replace($clean, $dirty, $encrypted_string)) , true );
    return $decrypted_string;
}


function dd( $arr, $is_bool = TRUE, $title = '' )
{
 if( $title )
 {
  echo '<strong>'.$title.'</strong>';
 }

 echo '<pre>';
 print_r( $arr );
 echo '</pre>';

 if( $is_bool )
  exit();
}
function pre($returnValue){
	echo '<pre>';
	print_r($returnValue);
	echo '</pre>';
}
?>